import coloredlogs, logging
from pathlib import Path

# Handle application logging
Path("log").mkdir(parents=True, exist_ok=True)

logging.basicConfig(filename="log/app.log",
                            filemode='a',
                            format='%(asctime)s,%(msecs)d %(name)s %(levelname)s %(message)s',
                            datefmt='%H:%M:%S',
                            level=logging.DEBUG)

coloredlogs.install(level='INFO')

logger = logging.getLogger(__name__)
logger.info("Starting the service")

#Start the service
import src.app