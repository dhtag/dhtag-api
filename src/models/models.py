from pydantic import BaseModel
from typing import List


class HashtagSearchResponse(BaseModel):
    """Response after a hashtag research, where:
    - search is the hashtag searched
    - nodesReached is the number of nodes reach during the search
    - instances is the list of instances (url of the instance) that have the hashtag researched
    """
    search: str
    nodesReached: int
    instances: List[str] = []

class HashtagList(BaseModel):
    """List of hashtag
        exemple: #avia, #mastodon, ...
    """
    hashtags: List[str] = []

class MessagePostResponse(BaseModel):
    """
    - status: request status (HTTP Code)
    - instances is a list of instances reached for the POST
    """
    status: str
    instances: List[str] = []
