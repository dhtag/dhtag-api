import uuid
import asyncio
import uvicorn

from fastapi import Depends, FastAPI, Header, HTTPException
from fastapi.openapi.utils import get_openapi
from .routers import messages, subscribers

from dotenv import load_dotenv, find_dotenv
import os

# Verify env vars and search if there is a .env vars in ./.env or in ./config/.env
from .config import verify_env_var, get_dht_instance

"""
This file initialize all the applications and configure global parameters. (Like event loops with uvloop)

"""
# Initialization
# Load parameters from env vars
api_key = os.getenv("API_KEY")
production = os.getenv("PRODUCTION")
rest_port= os.getenv("REST_PORT")
domain = os.getenv("LOCAL_DOMAIN")

# If port not set, use 8080 for REST service
if(rest_port == None):
    rest_port = 8080
else:
    rest_port = int(rest_port)


app = FastAPI()

dhtInstance = get_dht_instance()

async def get_token_header(x_token: str = Header(...)):
    """
    Verify API_KEY given by the clients at each request. If wrong api key is given, an error will be send to the client
    """
    if os.getenv("PRODUCTION") == "False" and x_token != api_key:
        raise HTTPException(status_code=400, detail="X-Token header invalid")

@app.on_event("startup")
async def startup_event():
    """
    Give the asyncio loop to the dht and start the dht. Rest API et DHT share the same event loop
    """
    dhtInstance.set_loop(asyncio.get_running_loop())
    await dhtInstance.run_node()

@app.on_event("shutdown")
def shutdown_event():
    """
    shutdown gracefully the DHT and REST api
    """
    dhtInstance.stop()

def custom_openapi():
    """
    Custom online doc generator for OpenAPI
    You can see docs here when you run the code: http://adress:port/docs or http://adress:port/redoc
    """
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Service Kademlia",
        version="1.0.0",
        description="Kademlia REST API documentation",
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://fastapi.tiangolo.com/img/logo-margin/logo-teal.png"
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema

app.openapi = custom_openapi

# Includes routes that are in "src/routers"
app.include_router(messages.router)
app.include_router(subscribers.router)

def run_server():
    """
    start the servers with env vars parameters
    """
    uvicorn.run("src.app:app", host=domain, port=rest_port, log_level="info", lifespan="on")

if __name__ == "src.app":
    run_server()

