from fastapi import APIRouter
from src.models.models import HashtagSearchResponse, MessagePostResponse, HashtagList
from ..config import get_dht_instance

router = APIRouter()
dhtInstance = get_dht_instance()

@router.get("/api/v1/dht/subscribers/{hashtag}", response_model=HashtagSearchResponse)
async def get_hashtagsub(hashtag: str):
    """
    Get the list of instances that are subscribed to the hashtag
    """
    instances = await dhtInstance.find_sub(hashtag)
    return {
        "search": hashtag,
        "nodesReached": len(instances),
        "instances": instances
    }

@router.post("/api/v1/dht/subscribers", response_model=MessagePostResponse)
async def send_message(message: HashtagList):
    """
    Add this instance to the list of instances that are subscribed to the hashtag
    """
    for tag in message.hashtags :
        await dhtInstance.add_subs(tag)
        print("Tag added as subscriber : " + tag)
    return {
        "message": ", ".join(message.hashtags),
        "status": "Send"
    }
