from fastapi import APIRouter
from src.models.models import HashtagSearchResponse, MessagePostResponse, HashtagList
from ..config import get_dht_instance

router = APIRouter()
dhtInstance = get_dht_instance()


@router.get("/api/v1/dht/messages/{hashtag}", response_model=HashtagSearchResponse)
async def get_hashtag(hashtag: str):
    """
    Get the list of instances that own messages having the hashtag
    """
    instances = await dhtInstance.find_owner(hashtag)
    return {
        "search": hashtag,
        "nodesReached": len(instances),
        "instances": instances
    }

@router.post("/api/v1/dht/messages", response_model=MessagePostResponse)
async def send_message(message: HashtagList):
    """
    Add this instance to the instances that own messages having the hashtag
    """
    for tag in message.hashtags :
        print("Tag added as owner : " + tag)
        await dhtInstance.add_owner(tag)
    return {
        "message": ", ".join(message.hashtags),
        "status": "Send"
    }
