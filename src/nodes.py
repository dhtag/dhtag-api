import socket
import yaml

# Read yml file and test all server one by one
def get_online_nodes(config_path='config/baseNodes.yml'):

    with open(config_path) as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
        #data = list(filter(lambda server: test_connection(server) == 0, data))
        return data

# Test if a server port is up
def test_connection(server):
    server_info = server.split(':')
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        s.connect((server_info[0], int(server_info[1])))
        s.shutdown(2)
        return 0
    except:
        return 1
