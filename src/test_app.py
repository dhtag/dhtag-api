import pytest
from multiprocessing import Process
from starlette.testclient import TestClient

from .app import run_server

@pytest.fixture
def server():
    proc = Process(target=run_server(), args=(), daemon=True)
    proc.start()
    yield
    proc.kill() # Cleanup after test

def test_ping(server):
    response = server.get("http://0.0.0.0:8080//api/v1/dht/messages/test")
    assert response.status_code == 200
