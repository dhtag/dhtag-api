import logging

from dotenv import load_dotenv, find_dotenv
import os
from .dht import Dht

logger = logging.getLogger(__name__)


def verify_env_var():
    """
    Verify env vars existence and raise errors if not set
    """
    load_dotenv(find_dotenv(".env"))
    load_dotenv(find_dotenv("config/.env"))

    if(os.getenv("LOCAL_DOMAIN") == None):
        raise Exception("No domain found, please add LOCAL_DOMAIN to your environment variables or in .env")

    if(os.getenv("PRODUCTION") == None):
        raise Exception("PRODUCTION environment variable not set, you need to set it to False or True before starting the service")

    if(os.getenv("REST_PORT") == None):
        logger.warn("REST_PORT environment variable not set, default port will be used (80)")

    if(os.getenv("DHT_PORT") == None):
        logger.warn("DHT_PORT environment variable not set, default port will be used (8989)")

    if(os.getenv("PRODUCTION") == "True" and os.getenv("API_KEY") == None):
        logger.warn("API_KEY environment variable not set, you need to set the API_KEY if you want to run the service in production mode")



verify_env_var()

# Create the DHT instance for global access accross all the code
if(os.getenv("DHT_PORT") != None):
    logger.info("Start DHT instance on port " + os.getenv("DHT_PORT"))
    dhtInstance = Dht(os.getenv("LOCAL_DOMAIN"), port=int(os.getenv("DHT_PORT")))
else:
    logger.info("Start DHT instance on port 8989")
    dhtInstance = Dht(os.getenv("LOCAL_DOMAIN"))

def get_dht_instance():
    """
    Return current DHT instance
    """
    return dhtInstance