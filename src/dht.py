import asyncio
import json
import os
import logging
from .nodes import get_online_nodes
from kademlia.network import Server


logger = logging.getLogger(__name__)

class Dht:

    def __init__(self, domain_name, node=Server(), loop=asyncio.get_event_loop(), ip="127.0.0.1", port=8989):
        # Create aself.node and start listening on port 5678
        self.node = node
        self.loop = loop
        self.ip = ip
        self.port = port
        self.domain_name = os.getenv("DOMAIN")
        return None

        # Bootstrap theself.node by connecting to other knownself.nodes, in this case
        # replace 123.123.123.123 with the IP of anotherself.node and optionally
        # give as many ip/port combos as you can for otherself.nodes.
        # await node.bootstrap([("123.123.123.123", 5678)]))

        # Lancement du serveur
        #await self.node.bootstrap([(self.ip, self.port)])

    def set_loop(self, loop):
        self.loop = loop

    async def connect_to_others_nodes(self):
        nodes = get_online_nodes()
        for server in nodes:
            server_info = server.split(':')
            await self.node.bootstrap([(server_info[0], int(server_info[1]))])

    def stop(self):
        logger.info("Stoping DHT")
        self.node.stop()

    async def run_node(self):
        await self.node.listen(self.port, os.getenv("LOCAL_DOMAIN"))
        await self.connect_to_others_nodes()

    # Get the list of instances that own messages having the hashtag
    async def find_owner(self, hashtag):
        try :
            list = await self.node.get(hashtag + "owns")
            print(list)
            print(type(list))
        except ValueError :
            logger.error("hashtag should be a string dummass")
        if (list is None):
            return []
        return json.loads(list)

    # Get the list of instances that are subscribed to the hashtag
    async def find_sub(self, hashtag):
        try :
            list = await self.node.get(hashtag + "subs")
        except ValueError :
            logger.error("hashtag should be string dummass")
        if (list is None):
            return []
        return json.loads(list)

    # Add this instance to the instances that own messages having the hashtag
    async def add_owner(self, hashtag):
        owner_list = self.domain_name
        try :
            return await self.node.add(hashtag + "owns", owner_list)
        except ValueError :
            logger.error("hashtag should be string dummass")
    # Add this instance to the list of instances that are subscribed to the hashtag
    async def add_subs(self, hashtag):
        subscriber_list = self.domain_name
        try :
            return await self.node.add(hashtag + "subs", subscriber_list)
        except ValueError :
            logger.error("hashtag should be string dummass")
