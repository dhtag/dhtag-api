# This Dockerfile is inspired by
# https://github.com/tiangolo/uvicorn-gunicorn-docker/blob/master/python3.7-alpine3.8/Dockerfile
# and https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker/blob/master/python3.7-alpine3.8/Dockerfile
# with updated version of python and alpine
# This config is not production ready

FROM python:3.7-alpine3.11

LABEL maintainer="Emily Delorme <emilydelormefr@gmail.com>"

WORKDIR /app/
COPY ./src /app/src
COPY main.py /app
COPY requirements.txt /app
COPY config/ /app/config

RUN apk add --no-cache --virtual .build-deps gcc libc-dev make git \
    && pip install --upgrade pip \
    && pip install -r requirements.txt \
    && apk del .build-deps gcc libc-dev make

ENV PYTHONPATH=/app

CMD ["python", "main.py"]
