# Classic installation

Install pythons libs

```bash
pip install -r requirements.txt
```

Before running the service, you need to configure it via the `.env` file located in `config/.env`, you can also set environment variable by hand.

More information [here](configuration.md).


Run the service with this:

```bash
python main.py
```

The service will be availiable at your domain with the port 80.

⚠️ Application will not work if you don't have the permission to run it under the port 80. You can use the port 8080 for testing.

## Guvicorn

> Gunicorn 'Green Unicorn' is a Python WSGI HTTP Server for UNIX. It's a pre-fork worker model. The Gunicorn server is broadly compatible with various web frameworks, simply implemented, light on server resources, and fairly speedy.

Guvicorn officiel website: https://gunicorn.org/

You can run the service with guvicorn if you want, but you have to manually install guvicorn before:

```bash
pip install gunicorn
```

Then run the server with **1** worker (the service need to be single thread, multithreading is currently not availible)

```bash
gunicorn -w 1 main:app
```
