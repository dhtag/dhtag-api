# Docker Installation

You have two way to handle the container, either with docker compose or with docker run. We encourage you to use docker-compose since is can be integrated in mastodon docker-compose file.

## Building the container

Build the container like this:

```bash
docker build . -t mastodon-kademlia
```

## Running the container

Run the container and exposing rest api with the port 8080 and 8989 for the DHT. In this we assumage that your `config/.env` file is fully configurated (more informations [here](configuration.md)):

```bash
docker run mastodon-kademlia \
-v "$(pwd)"/config:/app/config -v "$(pwd)"/log:/app/log \
-p 80:8080 -p 8989:8989
```

Without using `config/.env`

```bash
docker run mastodon-kademlia \
-e PRODUCTION=True -e LOCAL_DOMAIN=0.0.0.0 -e REST_PORT=80 \
-e DHT_PORT=8989 -e API_KEY=1234 \
-v "$(pwd)"/config:/app/config \
-v "$(pwd)"/log:/app/log \
-p 80:8080 -p 8989:8989
```


## Using Docker Compose

Using .env file located in `config/.env`:
```yml
version: '3.7'

services:
    mastodon-kademlia:
        build: .
        image: mastodon-kademlia
        ports:
            - '80:80'
            - '8989:8989'
        volumes:
            - ./config:/app/config
            - ./log:/app/log
```

Using docker environment only:

```yml
version: '3.7'

services:
    mastodon-kademlia:
        build: .
        image: mastodon-kademlia
        environment:
            - PRODUCTION=True
            - LOCAL_DOMAIN=0.0.0.0  # Need to be 0.0.0.0 for docker
            - REST_PORT=80
            - DHT_PORT=8989
            - API_KEY=1234
        ports:
            - '80:80'
            - '8989:8989'
        volumes:
            - ./config:/app/config
            - ./log:/app/log
```

### Exemple with a full docker-compose for mastodon

In this case the project in located in ../dhtag-api

```yml
version: '3'
services:

  db:
    restart: always
    image: postgres:9.6-alpine
    networks:
      - internal_network
    healthcheck:
      test: ["CMD", "pg_isready", "-U", "postgres"]
    volumes:
      - ./postgres:/var/lib/postgresql/data

  redis:
    restart: always
    image: redis:5.0-alpine
    networks:
      - internal_network
    healthcheck:
      test: ["CMD", "redis-cli", "ping"]
    volumes:
      - ./redis:/data

#  es:
#    restart: always
#    image: docker.elastic.co/elasticsearch/elasticsearch-oss:6.1.3
#    environment:
#      - "ES_JAVA_OPTS=-Xms512m -Xmx512m"
#    networks:
#      - internal_network
#    healthcheck:
#      test: ["CMD-SHELL", "curl --silent --fail localhost:9200/_cluster/health || exit 1"]
#    volumes:
#      - ./elasticsearch:/usr/share/elasticsearch/data

  web:
    build: .
    image: tootsuite/mastodon
    restart: always
    env_file: .env.production
    command: bash -c "rm -f /mastodon/tmp/pids/server.pid; bundle exec rails s -p 3000"
    networks:
      - external_network
      - internal_network
    healthcheck:
      test: ["CMD-SHELL", "wget -q --spider --proxy=off localhost:3000/health || exit 1"]
    ports:
      - "127.0.0.1:3000:3000"
    depends_on:
      - db
      - redis
#      - es
    volumes:
      - ./public/system:/mastodon/public/system

  streaming:
    build: .
    image: tootsuite/mastodon
    restart: always
    env_file: .env.production
    command: node ./streaming
    networks:
      - external_network
      - internal_network
    healthcheck:
      test: ["CMD-SHELL", "wget -q --spider --proxy=off localhost:4000/api/v1/streaming/health || exit 1"]
    ports:
      - "127.0.0.1:4000:4000"
    depends_on:
      - db
      - redis

  sidekiq:
    build: .
    image: tootsuite/mastodon
    restart: always
    env_file: .env.production
    command: bundle exec sidekiq
    depends_on:
      - db
      - redis
    networks:
      - external_network
      - internal_network
    volumes:
      - ./public/system:/mastodon/public/system

    mastodon-kademlia:
        build: ../dhtag-api
        image: mastodon-kademlia
        environment:
            - PRODUCTION=True
            - LOCAL_DOMAIN=0.0.0.0  # Need to be 0.0.0.0 for docker
            - REST_PORT=80
            - DHT_PORT=8989
            - API_KEY=1234
        ports:
            - '80:80'
            - '8989:8989'
        volumes:
            - ./config:/app/config
            - ./log:/app/log
## Uncomment to enable federation with tor instances along with adding the following ENV variables
## http_proxy=http://privoxy:8118
## ALLOW_ACCESS_TO_HIDDEN_SERVICE=true
#  tor:
#    image: sirboops/tor
#    networks:
#      - external_network
#      - internal_network
#
#  privoxy:
#    image: sirboops/privoxy
#    volumes:
#      - ./priv-config:/opt/config
#    networks:
#      - external_network
#      - internal_network

networks:
  external_network:
  internal_network:
    internal: true
```