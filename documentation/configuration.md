# Configuration files

## Environment variables

You can have to set different variable for your service before running it.

- **PRODUCTION** -> True or False (False desactivate API_KEY verification)
- **LOCAL_DOMAIN** -> the domain where the service run
- **REST_PORT** -> the port of your Rest API (default is 80)
- **DHT_PORT** -> the port of the DHT (default is 8989)
- **API_KEY** -> The api key for the REST api

## .env confguration file

You can set environment variable here `config/.env`

Exemple of configuration file:

```env
LOCAL_DOMAIN=0.0.0.0
PRODUCTION=True
REST_PORT=80
DHT_PORT=8989
API_KEY=1234
```

## Nodes configuration file

If you want to connect to others nodes at start, you can add them like this in `config/baseNodes.yml`

```yml
- domain:port
- domain2:port
```

⚠️ Having too much base nodes make the application longer to start since the application test all connections before starting the service.