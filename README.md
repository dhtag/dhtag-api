# DHTAG kademlia Service

## Introduction

This service is for creating bridge with Mastdon and the [DHT](https://en.wikipedia.org/wiki/Distributed_hash_table) (Distribued Hash Table)

## ⚠️ Production usage ⚠️

The project is not ready for a production usage currently.
If you want to run it anyway, we strongly encourage you to run it with docker behind a reverse-proxy and exposing **only** DHT port to internet and not the api (since the api is only for your local)

## Installation

We strongly encourage using Docker for this service and having a good API key for security reason. Using HTTPS is strongly recommanded for production usage.

### Without Docker

Read the documentation [here](documentation/classic_install.md).

### With Docker

Read the documentation [here](documentation/docker_install.md).

#### Configuration

Before running the project you have to create a configuration file and set environment variables.

This is documented [here](documentation/configuration.md).

## Technical Details

### Kademlia

**Documentation can be found at [kademlia.readthedocs.org](http://kademlia.readthedocs.org/).**

This library is an asynchronous Python implementation of the [Kademlia distributed hash table](http://en.wikipedia.org/wiki/Kademlia).  It uses the [asyncio library](https://docs.python.org/3/library/asyncio.html) in Python 3 to provide asynchronous communication.  The nodes communicate using [RPC over UDP](https://github.com/bmuller/rpcudp) to communiate, meaning that it is capable of working behind a [NAT](http://en.wikipedia.org/wiki/Network_address_translation).

This library aims to be as close to a reference implementation of the [Kademlia paper](http://pdos.csail.mit.edu/~petar/papers/maymounkov-kademlia-lncs.pdf) as possible.

### Uvicorn

**Uvicorn** is a lightning-fast "ASGI" server.

It runs asynchronous Python web code in a single process.

### FastAPI

FastAPI is a modern, fast (high-performance), web framework for building APIs with Python 3.6+.

The key features are:

* **Fast**: Very high performance, on par with **NodeJS** and **Go** (thanks to Starlette and Pydantic).
* **Fast to code**: Increase the speed to develop features by about 300% to 500% *.
* **Less bugs**: Reduce about 40% of human (developer) induced errors. *
* **Intuitive**: Great editor support. <abbr title="also known as auto-complete, autocompletion, IntelliSense">Completion</abbr> everywhere. Less time debugging.
* **Easy**: Designed to be easy to use and learn. Less time reading docs.
* **Short**: Minimize code duplication. Multiple features from each parameter declaration. Less bugs.
* **Robust**: Get production-ready code. With automatic interactive documentation.
* **Standards-based**: Based on (and fully compatible with) the open standards for APIs: <a href="https://github.com/OAI/OpenAPI-Specification" target="_blank">OpenAPI</a> (previously known as Swagger) and <a href="http://json-schema.org/" target="_blank">JSON Schema</a>.

<small>* estimation based on tests on an internal development team, building production applications.</small>